import java.io.PipedReader

fun main() {
    var f1 = Fraction(1.0, 3.0)
    var f2 = Fraction(6.0, 11.0)

    println(f1 == f2)

    println(f1)
    println(f2)
    println()
    println("Shekvecili : ${f1.shekveca()}")
    println("Shekvecili : ${f2.shekveca()}")
    println()
    println("wiladebis jami : ${add(f1,f2)}")

    println("wiladebis namravli" ${Gamravleba(f1,f2)}")
}

class Fraction(n: Double, d: Double) {
    public var numerator: Double = n
    public var denominator: Double = d
    fun pirveli(): Double {
        return numerator
    }

    fun meore(): Double {
        return denominator
    }
    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            if (numerator + other.denominator / denominator == other.numerator) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$numerator / $denominator"

    }
    fun shekveca(): String {
        var a: Double = 7.0
        while (a < denominator) {
            a = a + 7;
            if (denominator % a == 0.0 && numerator % a == 0.0) {
                numerator = numerator / a
                denominator = denominator / a
                a=7.0;
            }
        }
        return "$numerator / $denominator"
    }
}
fun add( f1: Fraction, f2: Fraction):String{
    var Pirv: Fraction = f1
    var Meore: Fraction = f2
    var a: Double = Pirv.meore()
    while (a <= Pirv.meore()*Meore.meore()){
        if (a % Pirv.meore() == 0.0 && a%Meore.meore() == 0.0)
            break
        else a=a+Pirv.meore()
    }
    return "${Pirv.pirveli()*(a/Pirv.meore())+Meore.pirveli()*(a/Meore.meore())} / $a"
}
fun multiply( f1: Fraction, f2: Fraction): String {
    var pirv: Fraction = f1
    var meore: Fraction = f2
    return "${Pirv.pirveli() * Meore.pirveli()} / ${Meore.pirveli() * Meore.meore()}"
}